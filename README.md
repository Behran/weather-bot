# @weather_stepan_bot

```
t.me/weather_stepan_bot
```

## Getting started

### Создание .env файла для окружения

```shell
    cp .env.example .env
```
### Запуск контейнеров

```shell
    docker-compose up -d
```

### Вход в контейнер

```shell
docker exec -it weather-bot-app-1 bash
```

### Запуск приложения

```shell
go run cmd/app/main.go
```