FROM golang:1.21 AS builder

WORKDIR /go/src/app/
COPY src/ .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o app cmd/app/main.go && chmod +x app

COPY bin/ .

FROM alpine:3.13.6

WORKDIR /usr/local/bin

COPY --from=builder /go/src/app/app app
COPY --from=builder /go/src/app/bin sunrise

ENTRYPOINT ["/usr/local/bin/app"]

EXPOSE 80