package facade

import (
	"weather-bot/internal/factory"
)

// Services ...
func Services() *factory.ServiceFactory { return factory.Services }
