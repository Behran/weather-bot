package bot

import (
	"context"
	"log"

	"weather-bot/internal/config"
	"weather-bot/internal/facade"
	"weather-bot/internal/model"
	"weather-bot/pkg/gismeteo"
	"weather-bot/pkg/logger"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"

	"go.uber.org/fx"
	"go.uber.org/zap"
)

// Telegram ...
type Telegram struct {
	logger *zap.Logger
	config config.Config
}

const (
	weatherCurrentData  = "1"
	weatherForecastData = "2"
)

var (
	TelegramClient *Telegram
	FirstMenu      = tgbotapi.NewInlineKeyboardRow(tgbotapi.NewInlineKeyboardButtonData("Прогноз на 9 часов", weatherForecastData))
	SecondMenu     = tgbotapi.NewInlineKeyboardRow(tgbotapi.NewInlineKeyboardButtonData("Текущая погода", weatherCurrentData))
	FirstStartMenu = tgbotapi.NewReplyKeyboard(
		tgbotapi.NewKeyboardButtonRow(tgbotapi.NewKeyboardButton("🇷🇺 Русский")),
		tgbotapi.NewKeyboardButtonRow(tgbotapi.NewKeyboardButton("🇬🇧 English")))
	//SecondStartMenu  = tgbotapi.NewReplyKeyboard(tgbotapi.NewKeyboardButtonRow(tgbotapi.NewKeyboardButton("")))
	ButtonMenuFirst = tgbotapi.NewReplyKeyboard(
		//tgbotapi.NewKeyboardButtonRow(tgbotapi.NewKeyboardButton("☀️ Погода сейчас")),
		tgbotapi.NewKeyboardButtonRow(tgbotapi.NewKeyboardButton("⚙️ Настройки")))
	ButtonMenuSecond = tgbotapi.NewReplyKeyboard(
		tgbotapi.NewKeyboardButtonRow(
			tgbotapi.NewKeyboardButton("🌏 Изменить город")),
		tgbotapi.NewKeyboardButtonRow(
			tgbotapi.NewKeyboardButton("🇷🇺/🇬🇧 Изменить язык")),
		tgbotapi.NewKeyboardButtonRow(
			tgbotapi.NewKeyboardButton("↩️ Назад")),
	)
	city       int
	weatherNow string
)

func NewTelegram(config config.Config, logger *zap.Logger) *Telegram {
	TelegramClient = &Telegram{
		logger: logger,
		config: config,
	}
	return TelegramClient
}

// Start ...
func Start(lc fx.Lifecycle, telegram *Telegram) error {
	lc.Append(
		fx.Hook{
			OnStart: func(ctx context.Context) error {
				logger.Logger.Info("on start")
				bot, err := tgbotapi.NewBotAPI(telegram.config.App.Token)
				if err != nil {
					logger.Logger.Error("error send message", zap.Error(err))
				}

				bot.Debug = true

				log.Printf("Authorized on account %s", bot.Self.UserName)

				u := tgbotapi.NewUpdate(0)
				u.Timeout = 60
				updates := bot.GetUpdatesChan(u)

				for update := range updates {
					switch {
					case update.Message != nil:
						if update.Message != nil { // If we got a message
							log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)
							//TODO:: сохраняем информацию по пользователю
							user, err := facade.Services().User().UpsertUserInfo(ctx, model.UserAttributes{Name: update.Message.From.UserName})
							if err != nil {
								logger.Logger.Error("fail save user info", zap.Error(err))
							}
							log.Printf("user: %+v", user)
							msg := tgbotapi.NewMessage(update.Message.Chat.ID, update.Message.Text)
							g := gismeteo.NewGismeteo(telegram.config.App.GisToken)
							switch update.Message.Text {
							case "/start":
								msg.Text = "Выберите язык / Choose language"
								msg.ReplyMarkup = FirstStartMenu
								_, err := bot.Send(msg)
								if err != nil {
									logger.Logger.Error("error send message", zap.Error(err))
									msg.Text = "Извините, в данный момент бот не доступен 🌚"
								}
							case "🇷🇺 Русский":
								msg.ReplyMarkup = ButtonMenuFirst
								msg.Text = "Напишите название населенного пункта"
								_, err := bot.Send(msg)
								if err != nil {
									logger.Logger.Error("error send message", zap.Error(err))
									msg.Text = "Извините, в данный момент бот не доступен 🌚"
								}
							case "☀️ Погода сейчас":
							case "⚙️ Настройки":
								msg.ReplyMarkup = ButtonMenuSecond
								msg.Text = "Открываю настройки"
								_, err := bot.Send(msg)
								if err != nil {
									logger.Logger.Error("error send message", zap.Error(err))
									msg.Text = "Извините, в данный момент бот не доступен 🌚"
								}
							case "🌏 Изменить город":
							case "🇷🇺/🇬🇧 Изменить язык":
								msg.Text = "Выберите язык / Choose language"
								msg.ReplyMarkup = FirstStartMenu
								_, err := bot.Send(msg)
								if err != nil {
									logger.Logger.Error("error send message", zap.Error(err))
									msg.Text = "Извините, в данный момент бот не доступен 🌚"
								}
							case "↩️ Назад":
								msg.ReplyMarkup = ButtonMenuFirst
								_, err := bot.Send(msg)
								if err != nil {
									logger.Logger.Error("error send message", zap.Error(err))
									msg.Text = "Извините, в данный момент бот не доступен 🌚"
								}
							default:
								city, err = g.SearchCityByName(update.Message.Text)
								if err != nil {
									logger.Logger.Error("error send message", zap.Error(err))
									msg.Text = "Извините, в данный момент бот не доступен 🌚"
								}
								weather, err := g.WeatherCurrent(city)
								if err != nil {
									logger.Logger.Error("error send message", zap.Error(err))
									msg.Text = "Извините, в данный момент бот не доступен 🌚"
								}
								if weather.Meta.Code == "200" {
									weatherNow = gismeteo.Description(weather)
									msg.Text = weatherNow
									msg.ReplyMarkup = tgbotapi.NewInlineKeyboardMarkup(FirstMenu)

								} else {
									msg.Text = "Такого города не существует"
								}
								if _, err := bot.Send(msg); err != nil {
									logger.Logger.Error("error send message", zap.Error(err))
									msg.Text = "Извините, в данный момент бот не доступен 🌚"
								}
							}

						}

					case update.CallbackQuery != nil:
						g := gismeteo.NewGismeteo(telegram.config.App.GisToken)
						query := update.CallbackQuery
						var msg tgbotapi.EditMessageTextConfig
						message := query.Message
						if query.Data == weatherCurrentData {
							weather, _ := g.WeatherCurrentForFuture(city)
							text := gismeteo.ResponseForFuture(weather)
							msg = tgbotapi.NewEditMessageTextAndMarkup(message.Chat.ID, message.MessageID, text, tgbotapi.NewInlineKeyboardMarkup(SecondMenu))
						} else if query.Data == weatherForecastData {
							msg = tgbotapi.NewEditMessageTextAndMarkup(message.Chat.ID, message.MessageID, weatherNow, tgbotapi.NewInlineKeyboardMarkup(FirstMenu))
						}
						_, err := bot.Send(msg)
						if err != nil {
							logger.Logger.Error("error send message", zap.Error(err))
						}

					}

				}
				return nil
			},
			OnStop: func(ctx context.Context) error {
				telegram.logger.Info("on stop")
				return nil
			},
		},
	)
	return nil
}
