package config

import (
	"os"
)

// Config ...
type Config struct {
	App      App
	Database Database
}

// App ...
type App struct {
	Token    string
	Port     string
	GisToken string
}

// Database ...
type Database struct {
	Mongo map[int]Mongo
}

// Mongo ...
type Mongo struct {
	Dsn      string
	Database string
}

const UserSettingsConnect = iota + 1

// New ...
func New() Config {
	return Config{
		App: App{
			Port:     os.Getenv("APP_PORT"),
			Token:    os.Getenv("APP_BOT_TOKEN"),
			GisToken: os.Getenv("X_GISMETEO_TOKEN"),
		},
		Database: Database{
			Mongo: map[int]Mongo{
				UserSettingsConnect: {
					Dsn:      os.Getenv("DB_MONGO_DSN"),
					Database: os.Getenv("DB_MONGO_DATABASE"),
				},
			},
		},
	}
}
