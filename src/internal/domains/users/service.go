package users

import (
	"context"

	"weather-bot/internal/domains/users/contract"
	"weather-bot/internal/model"
)

// Service ...
type Service struct {
	r contract.IUserRepository
}

func New(r contract.IUserRepository) *Service { return &Service{r: r} }

// UpsertUserInfo ...
func (s Service) UpsertUserInfo(ctx context.Context, data model.UserAttributes) (model.UserAttributes, error) {
	return s.r.Upsert(ctx, data)
}
