package repositories

import (
	"context"

	"weather-bot/internal/config"
	"weather-bot/internal/database"
	"weather-bot/internal/model"

	"go.mongodb.org/mongo-driver/mongo"
)

// UserRepository ...
type UserRepository struct {
	pool *mongo.Database
}

// NewUserRepository ...
func NewUserRepository(manager *database.ConnectManager) (*UserRepository, error) {
	var err error

	storage := new(UserRepository)
	storage.pool, err = manager.ConnectMongo(storage)
	if err != nil {
		return nil, err
	}
	return storage, nil
}

// Upsert ...
func (r UserRepository) Upsert(ctx context.Context, data model.UserAttributes) (model.UserAttributes, error) {
	return model.UserAttributes{}, nil
}

// ConnectName ...
func (UserRepository) ConnectName() int { return config.UserSettingsConnect }
