package contract

import (
	"context"

	"weather-bot/internal/model"
)

// IUserRepository ...
type IUserRepository interface {
	Upsert(ctx context.Context, data model.UserAttributes) (model.UserAttributes, error)
}
