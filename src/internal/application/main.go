package application

import (
	"weather-bot/internal/bot"
	"weather-bot/internal/config"
	"weather-bot/internal/database"
	"weather-bot/internal/factory"
	"weather-bot/internal/http/rest"
	"weather-bot/pkg/logger"

	"github.com/fasthttp/router"
	"go.uber.org/fx"
)

// Containers DI containers for Bot application ...
func Containers() *fx.App {
	return fx.New(
		fx.Provide(
			logger.New,
			config.New,
			router.New,
			database.NewConnectManager,
			factory.New,
			rest.NewServer,
			bot.NewTelegram,
		),
		fx.Invoke(
			database.InitConnections, // init connection database ...
			factory.InitServices,     // create domain services ...
			rest.RegisterRoutes,      // registration handle routes ...
			rest.StartServer,         // start http server ...
			bot.Start,                // start bot stream ...
		),
	)
}
