package instances

import (
	"context"
	"fmt"
	"time"

	"weather-bot/internal/config"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

// Mongo ...
type Mongo struct {
	config config.Database
	pool   map[int]*mongo.Database
}

// NewMongo ...
func NewMongo(config config.Database) *Mongo {
	return &Mongo{
		config: config,
		pool:   make(map[int]*mongo.Database),
	}
}

// Init ...
func (m *Mongo) Init() error {
	for k := range m.config.Mongo {
		connect, err := m.create(m.config.Mongo[k])
		if err != nil {
			return err
		}
		m.append(k, connect)
	}
	return nil
}

// create ...
func (m Mongo) create(config config.Mongo) (*mongo.Database, error) {
	opts := options.Client().
		SetMaxPoolSize(300).
		SetConnectTimeout(5 * time.Minute).
		ApplyURI(config.Dsn)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, opts)
	if err != nil {
		return nil, err
	}
	if err := client.Ping(ctx, readpref.Primary()); err != nil {
		return nil, err
	}
	return client.Database(config.Database), nil
}

// Connect ...
func (m Mongo) Connect(name int) (*mongo.Database, error) {
	connect, exist := m.pool[name]
	if !exist {
		return nil, fmt.Errorf("connect mongo `%d` not found", name)
	}
	return connect, nil
}

// Disconnect ...
func (m *Mongo) Disconnect(ctx context.Context) error {
	for k := range m.pool {
		if err := m.pool[k].Client().Disconnect(ctx); err != nil {
			return err
		}
	}
	return nil
}

// Status ...
func (m *Mongo) Status() error {
	for k := range m.pool {
		if err := m.pool[k].Client().Ping(context.Background(), readpref.Primary()); err != nil {
			return err
		}
	}
	return nil
}

// append ...
func (m *Mongo) append(key int, connect *mongo.Database) { m.pool[key] = connect }
