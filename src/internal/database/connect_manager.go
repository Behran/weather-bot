package database

import (
	"context"

	"weather-bot/internal/config"
	"weather-bot/internal/database/instances"

	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/fx"
	"golang.org/x/sync/errgroup"
)

// ConnectManager ...
type ConnectManager struct {
	mongo *instances.Mongo
}

// NewConnectManager ...
func NewConnectManager(config config.Config) *ConnectManager {
	return &ConnectManager{
		mongo: instances.NewMongo(config.Database),
	}
}

// InitConnections ...
func InitConnections(lc fx.Lifecycle, manager *ConnectManager) error {
	var eg errgroup.Group

	for _, instance := range []func() error{
		manager.mongo.Init,
	} {
		eg.Go(instance)
	}
	lc.Append(fx.Hook{
		OnStop: func(ctx context.Context) error {
			return manager.DisconnectMongo(ctx)
		},
	})
	return eg.Wait()
}

// ConnectMongo ...
func (c ConnectManager) ConnectMongo(client IClient) (*mongo.Database, error) {
	return c.mongo.Connect(client.ConnectName())
}

// DisconnectMongo ...
func (c ConnectManager) DisconnectMongo(ctx context.Context) error {
	return c.mongo.Disconnect(ctx)
}
