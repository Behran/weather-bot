package factory

import (
	"weather-bot/internal/database"
	"weather-bot/internal/domains/users"
	"weather-bot/internal/domains/users/repositories"

	"golang.org/x/sync/errgroup"
)

// ServiceFactory ...
type ServiceFactory struct {
	connector *database.ConnectManager
	users     *users.Service
}

var Services *ServiceFactory

// New ...
func New(connector *database.ConnectManager) *ServiceFactory {
	Services = &ServiceFactory{
		connector: connector,
	}
	return Services
}

// InitServices ...
func InitServices(factory *ServiceFactory) error {
	var eg errgroup.Group

	for _, service := range []func() error{
		factory.createUserService,
	} {
		eg.Go(service)
	}
	return eg.Wait()
}

// createGeomagneticService...
func (factory *ServiceFactory) createUserService() error {
	r, err := repositories.NewUserRepository(factory.connector)
	if err != nil {
		return err
	}
	factory.users = users.New(r)
	return nil
}

// User ...
func (factory *ServiceFactory) User() *users.Service { return factory.users }
