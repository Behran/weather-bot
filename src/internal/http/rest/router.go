package rest

import (
	"weather-bot/internal/http/rest/handlers"

	"github.com/fasthttp/router"
)

// RegisterRoutes Router List ...
func RegisterRoutes(router *router.Router) {
	router.PanicHandler = handlers.Panic
}
