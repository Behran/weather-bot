package handlers

import (
	"weather-bot/pkg/logger"

	"github.com/valyala/fasthttp"
	"go.uber.org/zap"
)

// Panic ...
func Panic(ctx *fasthttp.RequestCtx, i any) {
	logger.Logger.Error("handle panic", zap.Any("panic", i))
}
