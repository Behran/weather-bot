package model

type Sunrise struct {
	Date string `json:"date"`
	Sun  struct {
		SunriseDt        string `json:"sunrise_dt"`
		SunsetDt         string `json:"sunset_dt"`
		TransitDt        string `json:"transit_dt"`
		LowTransitPrevDt string `json:"low_transit_prev_dt"`
		LowTransitNextDt string `json:"low_transit_next_dt"`
		DayLength        int    `json:"day_length"`
		DeltaDayLength   int    `json:"delta_day_length"`
		PolarDay         int    `json:"polar_day"`
		PolarNight       int    `json:"polar_night"`
	} `json:"sun"`
	Moon struct {
		MoonriseDt       string  `json:"moonrise_dt"`
		MoonsetDt        string  `json:"moonset_dt"`
		IllumFrac        float64 `json:"illum_frac"`
		MoonQuart        int     `json:"moon_quart"`
		MoonIcon         int     `json:"moon_icon"`
		NewmoonCurrent   int     `json:"newmoon_current"`
		NewmoonNextDate  string  `json:"newmoon_next_date"`
		FullmoonCurrent  int     `json:"fullmoon_current"`
		FullmoonNextDate string  `json:"fullmoon_next_date"`
	} `json:"moon"`
}
type WeatherForFuture struct {
	Meta struct {
		Message string `json:"message"`
		Code    string `json:"code"`
	} `json:"meta"`
	Response []struct {
		Precipitation struct {
			TypeExt    interface{} `json:"type_ext"`
			Intensity  int         `json:"intensity"`
			Correction interface{} `json:"correction"`
			Amount     float64     `json:"amount"`
			Duration   int         `json:"duration"`
			Type       int         `json:"type"`
		} `json:"precipitation"`
		Pressure struct {
			HPa     int     `json:"h_pa"`
			MmHgAtm int     `json:"mm_hg_atm"`
			InHg    float64 `json:"in_hg"`
		} `json:"pressure"`
		Humidity struct {
			Percent int `json:"percent"`
		} `json:"humidity"`
		Icon string `json:"icon"`
		Gm   int    `json:"gm"`
		Wind struct {
			Direction struct {
				Degree int `json:"degree"`
				Scale8 int `json:"scale_8"`
			} `json:"direction"`
			Speed struct {
				KmH int `json:"km_h"`
				MS  int `json:"m_s"`
				MiH int `json:"mi_h"`
			} `json:"speed"`
		} `json:"wind"`
		Cloudiness struct {
			Type    int `json:"type"`
			Percent int `json:"percent"`
		} `json:"cloudiness"`
		Date struct {
			UTC            string      `json:"UTC"`
			TimeZoneOffset int         `json:"time_zone_offset"`
			Local          string      `json:"local"`
			HrToForecast   interface{} `json:"hr_to_forecast"`
			Unix           int         `json:"unix"`
		} `json:"date"`
		Phenomenon int `json:"phenomenon,omitempty"`
		Radiation  struct {
			UvbIndex int `json:"uvb_index"`
			UVB      int `json:"UVB"`
		} `json:"radiation"`
		City        int    `json:"city"`
		Kind        string `json:"kind"`
		Storm       bool   `json:"storm"`
		Temperature struct {
			Comfort struct {
				C float64 `json:"C"`
				F float64 `json:"F"`
			} `json:"comfort"`
			Water struct {
				C float64 `json:"C"`
				F float64 `json:"F"`
			} `json:"water"`
			Air struct {
				C float64 `json:"C"`
				F float64 `json:"F"`
			} `json:"air"`
		} `json:"temperature"`
		Description struct {
			Full string `json:"full"`
		} `json:"description"`
	} `json:"response"`
}
