package model

import "go.mongodb.org/mongo-driver/bson/primitive"

// UserAttributes ...
type UserAttributes struct {
	ID   primitive.ObjectID `bson:"_id"`
	Name string             `bson:"name"`
}
