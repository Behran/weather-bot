module weather-bot

go 1.21

require (
	github.com/fasthttp/router v1.4.20
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1
	github.com/valyala/fasthttp v1.49.0
	go.mongodb.org/mongo-driver v1.13.1
	go.uber.org/fx v1.20.0
	go.uber.org/zap v1.25.0
	golang.org/x/sync v0.6.0
)

require (
	github.com/andybalholm/brotli v1.0.5 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/klauspost/compress v1.16.3 // indirect
	github.com/montanaflynn/stats v0.0.0-20171201202039-1bf9dbcd8cbe // indirect
	github.com/savsgio/gotils v0.0.0-20230208104028-c358bd845dee // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/xdg-go/pbkdf2 v1.0.0 // indirect
	github.com/xdg-go/scram v1.1.2 // indirect
	github.com/xdg-go/stringprep v1.0.4 // indirect
	github.com/youmark/pkcs8 v0.0.0-20181117223130-1be2e3e5546d // indirect
	go.uber.org/dig v1.17.0 // indirect
	go.uber.org/multierr v1.10.0 // indirect
	golang.org/x/crypto v0.7.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/text v0.8.0 // indirect
)
