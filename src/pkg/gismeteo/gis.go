package gismeteo

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"strconv"
	"weather-bot/internal/model"
	"weather-bot/pkg/models"
)

type Gismeteo struct {
	token string
}

func NewGismeteo(token string) *Gismeteo {
	return &Gismeteo{
		token: token,
	}
}
func (g *Gismeteo) SearchCityByName(message string) (int, error) {
	req, err := http.NewRequest(http.MethodGet, "https://api.gismeteo.net/v2/search/cities/?query="+message, nil)

	if err != nil {
		return 0, err
	}
	req.Header.Add("X-Gismeteo-Token", g.token)
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return 0, err
	}
	defer res.Body.Close()
	var cities models.RsponseCities

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return 0, err
	}
	error := json.Unmarshal(body, &cities)
	if error != nil {
		return 0, err
	}
	if cities.Response.Total == 0 {
		return 0, errors.New("Города не существует")
	}
	return cities.Response.Items[0].ID, nil

}

func (g *Gismeteo) WeatherCurrent(city int) (models.Weather, error) {

	req, err := http.NewRequest(http.MethodGet, "https://api.gismeteo.net/v2/weather/current/"+strconv.Itoa(city)+"/", nil)
	if err != nil {
		return models.Weather{}, err
	}
	//sunrise.TimeInCity()
	req.Header.Add("X-Gismeteo-Token", g.token)
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return models.Weather{}, err
	}
	defer res.Body.Close()
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return models.Weather{}, err
	}
	var result models.Weather
	err = json.Unmarshal(body, &result)
	if err != nil {
		return models.Weather{}, err
	}

	return result, err

}

func (g *Gismeteo) WeatherCurrentForFuture(city int) (model.WeatherForFuture, error) {

	req, err := http.NewRequest(http.MethodGet, "https://api.gismeteo.net/v2/weather/forecast/"+strconv.Itoa(city)+"/?days=3", nil)
	if err != nil {
		return model.WeatherForFuture{}, err
	}
	req.Header.Add("X-Gismeteo-Token", g.token)
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return model.WeatherForFuture{}, err
	}
	defer res.Body.Close()
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return model.WeatherForFuture{}, err
	}
	var result model.WeatherForFuture
	err = json.Unmarshal(body, &result)
	if err != nil {
		return model.WeatherForFuture{}, err
	}
	return result, err

}
