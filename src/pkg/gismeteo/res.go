package gismeteo

import (
	"fmt"
	"log"
	"strconv"
	"time"

	"weather-bot/internal/model"
	"weather-bot/pkg/models"
)

var cloud = map[int]string{0: "☀️", 1: "🌤", 2: "🌥", 3: "☁️", 101: "☁️"}
var precipitation = map[int]string{1: "🌧", 2: "🌨", 3: "☄️"}

// Description ...
func Description(weather models.Weather) string {

	if weather.Response.Precipitation.Type == 0 {
		for key, value := range cloud {
			if weather.Response.Cloudiness.Type == key {
				// if time.Now().Add(time.Duration(weather.Response.Date.TimeZoneOffset)*time.Minute).String() >= 23 && time.Now().Add(time.Duration(weather.Response.Date.TimeZoneOffset)*time.Minute).String() <= 5 {
				// 	return message(weather, "🌙")
				// }
				return message(weather, value)
			}
		}
	} else if weather.Response.Storm {
		return message(weather, "🌩")
	} else {

		for key, value := range precipitation {
			if weather.Response.Precipitation.Type == key {
				return message(weather, value)
			}
		}
	}

	return ""
}
func message(weather models.Weather, emoji string) string {
	return emoji + weather.Response.Description.Full + "\n" + "Температура " + fmt.Sprintf("%+v", weather.Response.Temperature.Air.C)
}
func ResponseForFuture(weather model.WeatherForFuture) string {

	t := time.Now().Add(time.Duration(weather.Response[0].Date.TimeZoneOffset) * time.Minute)
	hour := t.Hour()
	var z int
	for i := 1; i <= len(weather.Response); i++ {
		if time.Unix(int64(weather.Response[i].Date.Unix), 0).Hour() >= time.Now().Hour() {
			z = i
			break
		}
	}
	log.Println(z)
	str := "Прогноз на 9 часов \n\n"
	for i := 0; i < 3; i++ {
		hour = hour + 3
		if hour > 24 {
			hour = hour - 24
		}
		if weather.Response[z].Precipitation.Type == 0 {
			for key, value := range cloud {
				if weather.Response[z].Cloudiness.Type == key {
					str += "Время " + strconv.Itoa(hour) + ": " + value + " " + weather.Response[z].Description.Full + "\n" + "Температура " + fmt.Sprintf("%+v", weather.Response[z].Temperature.Air.C) + "\n\n"
				}
			}
		} else if weather.Response[z].Storm {
			str += "Время " + strconv.Itoa(hour) + ": " + "🌩" + " " + weather.Response[z].Description.Full + "\n" + "Температура " + fmt.Sprintf("%+v", weather.Response[z].Temperature.Air.C) + "\n\n"
		} else {

			for key, value := range precipitation {
				if weather.Response[z].Precipitation.Type == key {
					str += "Время " + strconv.Itoa(hour) + ": " + value + " " + weather.Response[z].Description.Full + "\n" + "Температура " + fmt.Sprintf("%+v", weather.Response[z].Temperature.Air.C) + "\n\n"
				}
			}
		}

		z++
	}
	return str
}
