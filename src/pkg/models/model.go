package models

type City struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}
type RsponseCities struct {
	Meta struct {
		Message string `json:"message"`
		Code    string `json:"code"`
	} `json:"meta"`
	Response struct {
		Items []City `json:"items"`
		Total int    `json:"total"`
	} `json:"response"`
}

type Weather struct {
	Meta struct {
		Message string `json:"message"`
		Code    string `json:"code"`
	} `json:"meta"`
	Response Response `json:"response"`
}
type Response struct {
	Precipitation Precipitation `json:"precipitation"`
	Pressure      Pressure      `json:"pressure"`
	Humidity      struct {
		Percent int `json:"percent"`
	} `json:"humidity"`
	Icon       string `json:"icon"`
	Gm         int    `json:"gm"`
	Wind       Wind   `json:"wind"`
	Cloudiness struct {
		Type    int `json:"type"`
		Percent int `json:"percent"`
	} `json:"cloudiness"`
	Date       Date `json:"date"`
	Phenomenon int  `json:"phenomenon"`
	Radiation  struct {
		UvbIndex interface{} `json:"uvb_index"`
		UVB      interface{} `json:"UVB"`
	} `json:"radiation"`
	City        int         `json:"city"`
	Kind        string      `json:"kind"`
	Storm       bool        `json:"storm"`
	Temperature Temperature `json:"temperature"`
	Description struct {
		Full string `json:"full"`
	} `json:"description"`
}
type Wind struct {
	Direction struct {
		Degree int `json:"degree"`
		Scale8 int `json:"scale_8"`
	} `json:"direction"`
	Speed struct {
		KmH int `json:"km_h"`
		MS  int `json:"m_s"`
		MiH int `json:"mi_h"`
	} `json:"speed"`
}
type Precipitation struct {
	TypeExt    interface{} `json:"type_ext"`
	Intensity  int         `json:"intensity"`
	Correction bool        `json:"correction"`
	Amount     float64     `json:"amount"`
	Duration   int         `json:"duration"`
	Type       int         `json:"type"`
}
type Pressure struct {
	HPa     int     `json:"h_pa"`
	MmHgAtm int     `json:"mm_hg_atm"`
	InHg    float64 `json:"in_hg"`
}
type Date struct {
	UTC            string      `json:"UTC"`
	Local          string      `json:"local"`
	TimeZoneOffset int         `json:"time_zone_offset"`
	HrToForecast   interface{} `json:"hr_to_forecast"`
	Unix           int         `json:"unix"`
}
type Temperature struct {
	Comfort struct {
		C float64 `json:"C"`
		F float64 `json:"F"`
	} `json:"comfort"`
	Water struct {
		C float64 `json:"C"`
		F float64 `json:"F"`
	} `json:"water"`
	Air struct {
		C float64 `json:"C"`
		F float64 `json:"F"`
	} `json:"air"`
}
